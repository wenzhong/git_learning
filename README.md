Understand Git: ameatur's study
======

## Basic 
First, create a repo by using `git init git_learning`. And during the composition of this post, I have done some commits. Show the commit history to get contents of different patches.

## Branching
A branch in Git is simply a lightweight movable pointer to one of these commits.

### Merge
From one branch, use `git merge $FROM_BRANCH` to merge

```
    $ git branch master
    $ git merge try_branch
    $ git log --graph --pretty='%h %s'
    *   3729056 merge the try_branch branch
    |\
    | * 802e6ea first commit on try_branch
    * | 4292dd0 add example of pretty print in git log
    * | 986eda3 add useful git log options usage
    |/
    * 9b7dcc9 add some further change
    * 6e2494b init commit
```

I try to find a way to show the branch of each commit but fail. Later I realize that because branching is cheap and Git encourage user to create/remove branch frequently, and a branch HEAD is only a pointer to a specific commit, so there is no persistent way to track a branch. (Please let me know if that's incorrect)

### Resolving Conflicts
* As other version control system does, Git adds standard conflict-resolution markers to the files that have conflicts. After we fully removed the <<<<<<<, =======, >>>>>>> lines and resolve each of these section in each conflicted file, use `git add` on each file to mark it as resolved. And use `git commit` to commit staged changes.

* Note: in commit, we can modify that message with details about how we resolved the merge if we think it would be helpful to others looking at this merge in the future — why we did what we did, if it’s not obvious.

### Branch Management
* To see the last commit on each branch, use `git branch -v`
* Another useful option to figure out what state your branches are in is to filter output from `git branch -v` to branches that you have or have not yet merged into the branch you’re currently on. The useful --merged and --no-merged options have been available in Git since version 1.5.6 for this purpose. To see which branches are already merged into the branch you’re on, you can run `git branch --merged`

```
    $ git branch --merged
    * master
      try_branch
```

Because I have merged "try_branch" branch, so I see it now. How about this?

```
    $ git checkout -b hotfix
    $ git branch --no-merged
    $
```

No branch is un-merged? Why?

The reason is that we just create a branch and no commit on it, so both the HEAD pointer of master branch and hotfix branch are pointing to the same commit 3729056.

```
    $ git branch -v
      hotfix     3729056 merge the try_branch branch
    * master     3729056 merge the try_branch branch
      try_branch 802e6ea first commit on try_branch
```

now do something on hotfix branch. And rerun the `git branch --no-merged`

```
    $ git commit -a -m "apply a hotfix"
    [hotfix f7963a5] apply a hotfix
     1 file changed, 61 insertions(+)
    $ git co master
    Switched to branch 'master'
    $ git branch --no-merged
    hotfix
```

Good, now merge the new commit.
```
    $ git merge hotfix
    Auto-merging README.md
    CONFLICT (content): Merge conflict in README.md
    Automatic merge failed; fix conflicts and then commit the result.
    $ vim README.md
    $ git add README.md
    $ git commit -m "merge hotfix"
    [master cc87eec] merge hotfix
    $  git log --graph --pretty='%h %s'
    *   cc87eec merge hotfix
    |\
    | * 75828cc apply another hotfix
    | * f7963a5 apply a hotfix
    * | ec00aea apply change on master
    |/
    *   3729056 merge the try_branch branch
    |\
    | * 802e6ea first commit on try_branch
    * | 4292dd0 add example of pretty print in git log
    * | 986eda3 add useful git log options usage
    |/
    * 9b7dcc9 add some further change
    * 6e2494b init commit
```

It’s important to remember when you’re doing all this that these branches are completely local. When you’re branching and merging, everything is being done only in your Git repository — no server communication is happening.

### Remote Branches
This used to cause a few headache. Let's add a remote repo (to bitbucket.org).

    git remote add origin ssh://git@bitbucket.org/wenzhong/git_learning.git

And push all refs to this origin after creating an empty project on bitbucket.

```
    $ git push -u origin --all
    Warning: Permanently added the RSA host key for IP address '131.103.20.168' to the list of known hosts.
    Counting objects: 30, done.
    Delta compression using up to 8 threads.
    Compressing objects: 100% (20/20), done.
    Writing objects: 100% (30/30), 4.44 KiB, done.
    Total 30 (delta 9), reused 0 (delta 0)
    To ssh://git@bitbucket.org/wenzhong/git_learning.git
     * [new branch]      hotfix -> hotfix
     * [new branch]      master -> master
     * [new branch]      try_branch -> try_branch
    Branch hotfix set up to track remote branch hotfix from origin.
    Branch master set up to track remote branch master from origin.
    Branch try_branch set up to track remote branch try_branch from origin.
```

And let's pretend there're other collaborators who clone this code and push some update. I check out this code to another location (how about call my first local repo "repo1" and this new local repo "repo2"?)in my laptop. And add some changes.

    $ git commit -a -m "move the tips section to the end of this readme file"
    [master 93965e6] move the tips section to the end of this readme file
     1 file changed, 48 insertions(+), 47 deletions(-)
    $ git push origin
    To git@bitbucket.org:wenzhong/git_learning.git
       cc87eec..93965e6  master -> master


Now, there're 2 different states. 1 state from remote repo (bitbucket.org) and it has been updated by repo2. 1 state from repo1, which is identical with the initial state on remote repo before repo2 push his changes.

repo1 know nothing about repo2, now do some change on "repo1". Now I want to push my work to the remote repo.

### Synchronize work
Now I run `git fetch origin` from repo1. 
This command looks up which server origin is (in this case, it’s bitbucket.org), fetches any data from it that I don’t yet have, and updates my local database, moving my origin/master pointer to its new, more up-to-date position.

At this time, there're still two branch for repo1 -- origin/master, local/master. They are not the same. origin/master include changes from repo2. local/mastera include changes from repo1, they are not pushed yet.

That means we get a reference to origin's master branch locally.

But now I want to share, push it up to the remote. my local branches aren't automatically synchronized to the remotes I write to -- I have to explicitly push the branch.

Now use `git push origin master`. Note that `master` is the branch name of my local branch. You can also use `git push origin master:new_master` to create a new_master branch on remote "origin".  Next time, when repo2 fetches from server, they will get a references to where the server's version of new_master is under the remote branch origin/new_master.
    
    $ git push origin master:new_master
    ...
    To ssh://git@bitbucket.org/wenzhong/git_learning.git
     * [new branch]      master -> new_master

Now, repo2 can fetch this new branch by `git fetch origin`

    $ git fetch origin
    From bitbucket.org:wenzhong/git_learning
     * [new branch]      new_master -> origin/new_master

It’s important to note that when you do a fetch that brings down new remote branches, you don’t automatically have local, editable copies of them. In other words, in this case, you don’t have a new new_master branch — you only have an origin/new_master pointer that you can’t modify.

    $ git checkout -b new_master origin/new_master
    Branch new_master set up to track remote branch new_master from origin.
    Switched to a new branch 'new_master'

Wait, what happen if I push the repo1/master to origin/master? I assume there would be conflicts.

```
    $ git push origin
    To ssh://git@bitbucket.org/wenzhong/git_learning.git
     ! [rejected]        master -> master (non-fast-forward)
    error: failed to push some refs to 'ssh://git@bitbucket.org/wenzhong/git_learning.git'
    hint: Updates were rejected because the tip of your current branch is behind
    hint: its remote counterpart. Merge the remote changes (e.g. 'git pull')
    hint: before pushing again.
    hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

Follow it's hint and run `git pull origin master`

```
    $ git pull origin master
    ...
    From ssh://bitbucket.org/wenzhong/git_learning
     * branch            master     -> FETCH_HEAD
    Updating cc87eec..93965e6
    error: Your local changes to the following files would be overwritten by merge:
        README.md
    Please, commit your changes or stash them before you can merge.
```

Git require that our repo should be clean before merging remote changes, so local repo will not corrupted.

```
    $ git commit -m "add syncing remote & local repo"
    [master 205e4ce] add syncing remote & local repo
     1 file changed, 43 insertions(+)
    $ git pull origin master
    rom ssh://bitbucket.org/wenzhong/git_learning
     * branch            master     -> FETCH_HEAD
    Auto-merging README.md
    CONFLICT (content): Merge conflict in README.md
    Automatic merge failed; fix conflicts and then commit the result.
```

That's expected, remember that in repo2, we move the tip section to the bottom. (Yes, if you check out commit 986eda3, tips are on top of this README file. and repo2 put it to the bottom at commit 93965e6). So resolve conflicts. and `git commit -a -m "merge changes from repo2 and apply my fix"`

```
    $ git push origin master
    ...
    ssh://git@bitbucket.org/wenzhong/git_learning.git
    93965e6..586c16f  master -> master
```

That's it.
To summarize, in repo1, we:
* Fetch change from origin/master (latest updated by repo2)
* try to automatically merge our local change (but could not)
* merge it locally
* push to origin/master

### Deleting Remote Branches
Now I think the branch "new_branch" have finished its duty, and I want to delete it on a remote server. I will use `git push origin :new_branch`.  A way to remember this command is by recalling the `git push [remotename] [local- branch]:[remotebranch]` syntax that we went over a bit earlier. If you leave off the `[localbranch]` portion, then you’re basically saying, "Take nothing on my side and make it be `[remotebranch]`." 

```
    $ git push origin :new_master
    To ssh://git@bitbucket.org/wenzhong/git_learning.git
     - [deleted]         new_master
```

## Tips and Tricks
### Inspect commits
There are many options can be used in `git log`. Some are extremely useful:
* git log --graph
* git log --stat
* git log --since="2013-10-01" --before="2014-03-01" --author=fwz
* git log --pretty="%h - %ad - %an - %s"

    986eda3 - Sat Mar 1 20:15:12 2014 +0800 - fwz - add useful git log options usage
    9b7dcc9 - Sat Mar 1 20:01:06 2014 +0800 - fwz - add some further change
    6e2494b - Sat Mar 1 19:44:56 2014 +0800 - fwz - init commit

### Useful option
* `git diff --cached` can be used to see diff in staging area.

### Rename a branch
* `git branch -m <oldname> <newname>`
* `git branch -m <newname>` , if you are renaming the current branch

### Auto completion
If you use the Bash shell, Git comes with a nice auto-completion script you can enable.  Get the latest git-completion.sh from [Github](https://raw.github.com/git/git/master/contrib/completion/git-completion.bash), put it in your HOME directory and put `source ~/git-completion.sh` to source it when you login.

This also works with options, which is probably more useful. For instance, if you’re running a git log command and can’t remember one of the options, you can start typing it and press Tab to see what matches:

    $ git log --s<tab>
    --shortstat  --since=  --src-prefix=  --stat   --summary

That’s a pretty nice trick and may save you some time and documentation reading.

### Aliases
Git doesn’t infer your command if you type it in partially. If you don’t want to type the entire text of each of the Git commands, you can easily set up an alias for each command using git config. Note: the global settings

git config global is under ~/.gitconfig. Here's my simple aliases.

    [user]
        email = wenzhong.work@gmail.com
        name = fwz
    [core]
        editor = /usr/local/bin/vim
    [alias]
        ci = commit
        co = checkout
        st = status
        br = branch
        unstage = reset HEAD --
        last = log -1 HEAD

